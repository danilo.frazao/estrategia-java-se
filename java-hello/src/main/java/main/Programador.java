package main.java.main;

public class Programador extends Funcionario{
    String linguagemDeProgramacao;

    public Programador(String nome, double salario) {
        super(nome, salario);
    }

    // Sobrescrita: Sobrescreve o método do pai
    @Override
    public double getBonificacao() {
        return this.getSalario() * 10;
    }
}
