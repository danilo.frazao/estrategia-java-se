package main.java.main;

public class EncapsulamentoStatic {
    static int a = 10;
    int b = 20;
    public static void main (String args []){
        Estudante e1 = new Estudante();
        Estudante e2 = new Estudante();

        e1.nome = "raphael";
        e2.nome = "max";
        Conta c1 = new Conta(e1);
        c1.depositar(100);
        System.out.println(c1.cliente.nome);
        System.out.println(c1.getSaldo());
        c1.depositar(50);
        System.out.println(c1.cliente.nome);
        System.out.println(c1.getSaldo());
        c1.sacar(30);
        System.out.println(c1.cliente.nome);
        System.out.println(c1.getSaldo());

        Estudante e3 = new Estudante();
        Estudante e4 = new Estudante();
        System.out.println("");
        int totalDeObjetosCriados = Estudante.contador;
        System.out.println("Quantos objetos foram criados?");
        System.out.println("Resposta: " + totalDeObjetosCriados);

        // Um método estático só consegue acessar atributos estáticos
        System.out.println("Atributo da classe: " + a);
        // Para um método estático acessar um atribulo do objeto é necessário primeiro criar o objeto
        System.out.println("Atributo do objeto: " + new EncapsulamentoStatic().b);

        EncapsulamentoStatic encapsulamentoStatic = new EncapsulamentoStatic();
        encapsulamentoStatic.olaMundo();
    }

    /**
     * Não é possível acessar os métodos de um objeto a partir dos métodos de classe (static), sem antes, criar esse objeto.
     * Os métodos do objeto sempre conseguem acessar os métodos de classe (static).
     * Isso acontece porque a classe sempre vem antes do objeto.
     * **/
    public void olaMundo(){
        System.out.println("");
        System.out.println("Atributo da classe: " + a);
        System.out.println("Atributo do objeto: " + b); //Não é necessário instanciar (criar)
    }
}
