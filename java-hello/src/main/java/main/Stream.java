package main.java.main;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;

public class Stream {
    public static void main (String args []){
        List<Conta> contas = new ArrayList<>();
        contas.add(new Conta(300));
        contas.add(new Conta(100));
        contas.add(new Conta(500));
        contas.add(new Conta(600));

        System.out.println("Primeiro");
        System.out.println(contas);

        System.out.println("Segundo");
        for (Conta c: contas) {
            System.out.println(c);
        }

        System.out.println("Terceiro");
        contas.forEach(new Consumer<Conta>() {
            @Override
            public void accept(Conta conta) {
                System.out.println(conta);
            }
        });

        System.out.println("Quarto");
        contas.forEach((c) -> {
            System.out.println(c);
        });

        System.out.println("Quinto");
        contas.forEach(c -> System.out.println(c));

        System.out.println("Sexto");
        contas.forEach(System.out::println); //Esse é o method reference

        System.out.println("Setimo");
        //Remove todos os elementos que estão maior do que 500
        contas.removeIf(c -> c.getSaldo() >= 500);
        contas.forEach(System.out::println);

        contas.add(new Conta(500));
        contas.add(new Conta(600));

        System.out.println("Oitavo");
        // Collections.sort(contas); //Como era antes
        // contas.sort(Comparator.naturalOrder()); //Agora
        contas.sort(Comparator.reverseOrder()); //Agora
        contas.forEach(System.out::println);

        // Para buscar o maior ou menor elemento
        System.out.println("Nono");
        Conta maior = contas.stream().max(Conta::compareTo).get();
        System.out.println(maior);
        Conta menor = contas.stream().min(Conta::compareTo).get();
        System.out.println(menor);

        // Uma lista com todos os saldos
        System.out.println("Decimo");
        List<Double> saldos = contas.stream().map(Conta::getSaldo).toList();
        System.out.println(saldos);

        // Soma total
        System.out.println("Decimo primeiro");
        BigDecimal somaTotal = contas
                .stream()
                .map(c -> new BigDecimal(c.getSaldo()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println("Soma 1: " + somaTotal);
        double sum = contas.stream().mapToDouble(Conta::getSaldo).sum();
        System.out.println("Soma 2: " + sum);
        double avg = contas.stream().mapToDouble(Conta::getSaldo).average().getAsDouble();
        System.out.println("Media: " + avg);

        System.out.println("Decimo segundo");
        List<Conta> contas_filtradas = contas.stream().filter(c -> c.getSaldo() >= 500).toList();
        contas_filtradas.forEach(System.out::println);

        System.out.println("Decimo terceiro");
        Conta conta1 = contas.stream().findFirst().get();
        System.out.println("1° da list: " + conta1);
        Conta conta2 = contas.stream().filter(c -> c.getSaldo() == 100).findFirst().get();
        System.out.println("1° com valor 100: " + conta2);

        System.out.println("Decimo terceiro"); //Apenas adicionando valores
        contas.add(new Conta(50));
        contas.add(new Conta(30));
        contas.add(new Conta(500));
        contas.add(new Conta(100));
        contas.forEach(System.out::println);

        System.out.println("Decimo quarto");
        List<Conta> contas_filtradas1 = contas
                .stream()
                .filter(c -> c.getSaldo() >= 100) // Maiores ou igual a 100
                .distinct() // Sem repetição (tem que implementar o equals)
                .sorted() // Ordenadas
                .toList();
        contas_filtradas1.forEach(System.out::println);
    }
}
