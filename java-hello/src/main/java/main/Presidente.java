package main.java.main;

public class Presidente extends FuncionarioAutenticavel {

    public Presidente(String nome, double salario, String senhaDeAutenticacao) {
        super(nome, salario, senhaDeAutenticacao);
    }

    // Sobrescrita: Sobrescreve o método do pai
    @Override
    public double getBonificacao() {
        return this.getSalario() * 5;
    }
}
