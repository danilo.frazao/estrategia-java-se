package main.java.main;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Lambda {
    public static void main(String[] args) {
        List<Integer> numeros = Arrays.asList(1, 2, 3, 4, 5);
        numeros.stream().filter(n -> n % 2 == 0).forEach(System.out::println);
        System.out.println("");

        OperacaoSoma soma = (x, y) -> x + y;
        System.out.println("Soma: " + soma.operar(3, 5)); // Saída: 8
        System.out.println("");

        Consumer<String> imprimir = s -> System.out.println(s);
        imprimir.accept("Hello, Lambda!"); // Saída: Hello, Lambda!
        System.out.println("");

        int valor = 10;
        Consumer<Integer> imprimirSoma = (x) -> System.out.println(x + valor);
        imprimirSoma.accept(5);
    }

    interface OperacaoSoma {
        int operar(int x, int y);
    }
}
