package main.java.main;

/**
 * A classe abstrata não pode mais ser instânciada
 * **/
public abstract class Funcionario {
    private String nome;
    private double salario;

    public Funcionario() {
    }

    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public double getSalario() {
        return salario;
    }

    public double getBonificacao(){
        return this.salario * 2;
    }
}
