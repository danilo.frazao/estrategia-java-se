package main.java.main;

public interface D extends B, C {
    default void m1() {
        B.super.m1();
    }
}
