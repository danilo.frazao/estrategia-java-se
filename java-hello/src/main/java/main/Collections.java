package main.java.main;

import java.util.*;

public class Collections {
    public static void main(String[] args){
        List idade1 = new ArrayList();
        idade1.add(10);
        idade1.add(10.5);
        idade1.add("rafa");
        idade1.add(10);
        idade1.add(true);
        System.out.println(idade1);
        System.out.println("");

        //List: aceita elementos repetidos, mas não é adequado para pesquisas
        List<Integer> idade2 = new ArrayList<Integer>();
        idade2.add(10);
        idade2.add(11);
        idade2.add(12);
        idade2.add(13);
        idade2.remove(1);
        System.out.println(idade2);
        System.out.println(idade2.contains(12));
        System.out.println("");

        //O set
        //Não tem elementos repetivos, não garante ordem de inserção, não tem índices
        //Mais adequado para pesquisas
        Set<String> nomes = new TreeSet<>();
        //Set<String> nomes = new HashSet<>();
        //Set<String> nomes = new LinkedHashSet<>(); //Mantém a ordem de inserção
        nomes.add("danilo");
        nomes.add("annita");
        nomes.add("danilo");
        nomes.add("danilo");
        System.out.println(nomes);
        System.out.println("");

        //Map: o valor pode ser repetido, desde que seja uma chave diferente
        Map<Integer, String> nomesPorId = new HashMap<>();
        nomesPorId.put(1, "danilo");
        nomesPorId.put(2, "anitta");
        nomesPorId.put(3, "guilherme");
        nomesPorId.put(1, "leandro");
        System.out.println(nomesPorId.get(1));
        System.out.println(nomesPorId);
        System.out.println("");
    }
}
