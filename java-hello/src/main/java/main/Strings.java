package main.java.main;

import java.util.HashSet;
import java.util.Set;

public class Strings {
    public static void main(String[] args) {
        // Collections
        Set<Object> hs = new HashSet<Object>();
        // O set não armazena objetos repetidos
        hs.add("Java");
        hs.add(new String("Java"));
        System.out.println(hs);

        String nome1 = "danilo";
        String nome2 = "danilo";
        System.out.println(nome1 == nome2);      //Endereço de memória é o mesmo?
        System.out.println(nome1.equals(nome2)); //Conteúdo é o mesmo?

        String nome3 = new String("danilo");
        String nome4 = new String("danilo");
        System.out.println(nome3 == nome4);      //Endereço de memória é o mesmo?
        System.out.println(nome3.equals(nome4)); //Conteúdo é o mesmo?
        System.out.println("");

        // Concatenação
        int A = 5, B = 4;
        System.out.printf("Soma é: %d\n", A + B);
        System.out.println("Soma é: " + A + B);
        System.out.println("");

        //String Builder
        var a = "danilo"; //A string é imutável
        a.concat(" frazão");
        System.out.println(a);

        var b = a.concat(" frazão");
        System.out.println(b);

        StringBuilder c = new StringBuilder("danilo");
        c.append(" frazão"); //String Builder é multável
        System.out.println(c);
    }
}
