package main.java.main;

/**
 * O código deve ser fechado para mudanças, porém aberto para extensões.
 * Toda vez que entrar um novo cargo não é necessário fazer mudanças. Porque todos são funcionários
 * **/
public class TotalizadorDeBonificacoes {
    private double totalGastoEmBonificacoes;

    public void calcularBonificacao (Funcionario funcionario) {
        this.totalGastoEmBonificacoes += funcionario.getBonificacao();
    }

    public double getTotalGastoEmBonificacoes() {
        return totalGastoEmBonificacoes;
    }
}
