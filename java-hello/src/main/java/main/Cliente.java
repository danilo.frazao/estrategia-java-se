package main.java.main;

public class Cliente implements Autenticavel{
    String nome;
    int idade;
    String senhaDeAutenticacao;

    public Cliente(String nome, String senhaDeAutenticacao) {
        this.nome = nome;
        this.senhaDeAutenticacao = senhaDeAutenticacao;
    }

    @Override
    public String getSenhaDeAutenticacao() {
        return this.senhaDeAutenticacao;
    }
}
