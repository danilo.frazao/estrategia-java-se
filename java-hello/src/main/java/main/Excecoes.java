package main.java.main;

public class Excecoes {
    public static void main (String args[]) {
        Matematica matematica = new Matematica();
        int[] numeros = new int[] { 10, 20, 30, 100 };
        int[] peso = new int[] { 2, 0, 4 };

        for (int indice = 0; indice < 4; indice++){
            try {
                matematica.dividir(numeros[indice], peso[indice]);
                // throw new Exception("Testando este erro");
            } catch (ArithmeticException erro) {
                // Somente trata os erros de aritimética
                 System.out.println("Erro de cálculo: " + erro.getMessage());
                 // erro.printStackTrace();
            } catch (ArrayIndexOutOfBoundsException erro) {
                // Somente trata os erros de utrapassagem do tamanho do vetor
                System.out.println("Erro de vetor: " + erro.getMessage());
                // erro.printStackTrace();
            } catch (Exception erro) { // O erro mais genérico de todos sempre fica ao final
                System.out.println("Erro de Genérico: " + erro.getMessage());
                // erro.printStackTrace(); // Serve para saber exatamento aonde ocorreu a exceção
            }
        }
        System.out.println("Mensagem final");
    }
}
