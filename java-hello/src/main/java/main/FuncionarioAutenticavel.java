package main.java.main;

public class FuncionarioAutenticavel extends Funcionario implements Autenticavel{
    private String senhaDeAutenticacao;

    public FuncionarioAutenticavel(String nome, double salario, String senhaDeAutenticacao) {
        super(nome, salario);
        this.senhaDeAutenticacao = senhaDeAutenticacao;
    }

    @Override
    public String getSenhaDeAutenticacao() {
        return senhaDeAutenticacao;
    }
}
