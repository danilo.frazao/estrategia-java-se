package main.java.main;

import java.util.Objects;

/**
 * Encapsulamento -> esconder detalhes da aplicação com os modificadores de acesso
 *
 * Tipos de acesso, do mais restrito para o menos restrito
 *
 * PRIVATE - Somente própria classe
 * DEFAULT - Somente classes do mesmo pacote
 * PROTECTED - Classes filhas, mesmo que não sejam do mesmo pacote
 * PUBLIC - Todas as classes podem acessar
 * **/

public class Conta implements Comparable<Conta> {
    private double saldo;
    public Estudante cliente;

    public Conta() {
    }


    public Conta(double saldo) {
        this.saldo = saldo;
    }
    public Conta(Estudante estudante) {
        this.cliente = estudante;
    }

    public void depositar(double valor){
        this.saldo += valor;
    }

    public void sacar(double valor) {
        this.saldo -= valor;
    }

    public double getSaldo() {
        return saldo;
    }

    @Override
    public String toString() {
        return "Conta {" +
                "saldo=" + saldo +
                '}';
    }

    @Override
    public int compareTo(Conta o) {
        return (int)(this.saldo - o.getSaldo());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Conta conta = (Conta) o;
        return Double.compare(conta.saldo, saldo) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(saldo);
    }
}
