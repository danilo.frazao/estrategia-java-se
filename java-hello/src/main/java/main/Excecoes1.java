package main.java.main;

public class Excecoes1 {
    public static void main(String args[]) throws Exception{
        System.out.println("Início");
        try {
            System.out.println("try");
            throw new Exception("O erro foi aqui!");
        } catch (Exception e) {
            System.out.println("catch: " + e.getMessage());
            //throw e;
        } finally {
            System.out.println("finally");
        }
        System.out.println("fim");
    }
}
