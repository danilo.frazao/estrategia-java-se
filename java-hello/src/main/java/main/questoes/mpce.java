package main.java.main.questoes;

public class mpce {
    public static int Funcao(int num, int resultado) {
        if (num < 1)
            return resultado;
        resultado += (num % 10);
        return Funcao(num / 10, resultado);
    }

    public static void main(String[] args) {
        int res = Funcao(17, 0);
        System.out.println("O resultado é: " + res);
    }

}
