package main.java.main.questoes;

public abstract class FCCQuestao23 {
    private static final double VALOR = 10;
    public abstract double soma(double n1, double n2);
    public abstract void exibeResultado();
    protected abstract double soma(double n1, double n2, double n3);
    // private abstract int multiplica(double n1, int n2); // Método abstrato não pode ser privado
    private double multiplica(double n1, double n2) {
        return n1 * n2;
    }

    public FCCQuestao23() {
    }
}
