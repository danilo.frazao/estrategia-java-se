package main.java.main.questoes;

public class FGVQuestao5 {
    public static void main(String[] args) {
        int a = 4, b = 16;
        String out = (a ^ b) == 0 ? "Sim" : (a & b) != 0
                ? "Talvez" : "Não";
        System.out.println(out);
    }
}
/*
a =   00100
b =   10000
xor = 10100 = 20
& =   00000 = 0
*/