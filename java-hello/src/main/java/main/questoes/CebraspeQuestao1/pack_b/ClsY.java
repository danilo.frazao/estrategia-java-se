package main.java.main.questoes.CebraspeQuestao1.pack_b;

public class ClsY implements IntfX {
    protected Double v=0.0;
    String p=null; //Por padrão assume o valor default ou package-private

    public ClsY () {
    }

    protected ClsY (Double x,String b) {
        v=x;
        p=b;
    }

    public void op_a(Double a) {
        v+=a;
    }

    public void op_b(Double b) {
        v-=b;
    }

    public void op_c() {
        v=0.0;
        p=null;
    }
}
