package main.java.main.questoes.CebraspeQuestao1.pack_a;

import main.java.main.questoes.CebraspeQuestao1.pack_b.ClsY;
import main.java.main.questoes.CebraspeQuestao1.pack_b.IntfX;

public class Main extends ClsY {
    public static void main(String[] args){
        IntfX a = new Main();
        ClsY b = new Main();
        Main c = new Main();
        System.out.println("Olá mundo");
        // O atributo está como default
        // Para ter acesso, é necessário colocar o atributo como public
        //b.p="****";

        // Não é possível acessar os métodos de um objeto a partir de métodos static (de classe).
        // Para ter acesso, é necessário colocá-lo, também, como static (mas vai aparecer outro erro dentro do método seguindo o mesmo raciocínio para resolver).
        // Ou, primeiro, deve-se instanciar a classe para ter o acesso.
            // new ClsY().op_c();
        //op_c();

        //Correto
        c.op_a(0.5,0.8);

        //A mesma situação que o caso anterior.
        //op_K(5.2);

        // Ele está pegando o método do IntfX que só aceita um parâmetro
        // Para corrigir:
            // Colocar a função abaixo com apenas um parâmetro
            // Implementar a sobreescrita no método op_a da classe Main com a mesma quantidade de parâmetros.
        //a.op_a(0.0,0.1);
    }
    public Main(){
        super(0.0,"");
    }

    public void op_a(Double e, Double f){
        v=e+f;
    }
    public void op_b(Double e, Double f){
        v=e+f;
    }
    public void op_K(Double d){
        v=d;
    }
}
