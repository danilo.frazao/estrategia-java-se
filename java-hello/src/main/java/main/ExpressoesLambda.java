package main.java.main;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@FunctionalInterface
interface MathOperation {
    int operation(int a, int b);
}

public class ExpressoesLambda {
    public static void main(String args[]) {
        // Sem Parâmetros
        Runnable runnable = () -> System.out.println("Hello, World!");
        runnable.run();

        // Com Um Parâmetro
        Consumer<String> consumer = s -> System.out.println(s);
        consumer.accept("teste1");

        // Com Múltiplos Parâmetros
        BiFunction<Integer, Integer, Integer> adder = (a, b) -> a + b;
        System.out.println(adder.apply(4, 5));

        // Uso de Expressões Lambda com Streams
        // Uso de lambda para filtrar e mapear elementos de uma lista
        List<String> names = Arrays.asList("Alice", "Bob", "Charlie", "David");
        List<String> filteredNames = names.stream()
                .filter(name -> name.startsWith("A"))
                .map(name -> name.toUpperCase())
                .collect(Collectors.toList());
        System.out.println(filteredNames); // Output: [ALICE]

        // Interfaces Funcionais
        // Você também pode definir suas próprias interfaces funcionais usando a anotação @FunctionalInterface
        // para garantir que a interface tenha exatamente um método abstrato.
        MathOperation addition = (a, b) -> a + b;
        System.out.println("Adição: " + addition.operation(5, 3)); // Output: Addition: 8
        MathOperation multiplication = (a, b) -> a * b;
        System.out.println("Multiplication: " + multiplication.operation(5, 3)); // Output: Multiplication: 15
    }
}
