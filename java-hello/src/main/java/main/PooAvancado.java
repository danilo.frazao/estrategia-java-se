package main.java.main;

/**
 * Classes Abstratas,
 * Herança,
 * Sobrecarga x Sobrescrita,
 * Acoplamento x Coesão,
 * Herança Simples x Herança Múltipla,
 * Interfaces,
 * Polimorfismo,
 * Default Methods
 * **/

public class PooAvancado {
    public static void main(String args[]){
        System.out.println("Primeiro");
        Funcionario f2 = new Programador("matheus", 100);
        // Multiplica por 10 (Sobrescrita) em vez de ser por 2 do Funcionário
        // Lembrando que não consegue chamar a linguagem de programação
        System.out.println(f2.getBonificacao());

        System.out.println("");

        System.out.println("Segundo");
        Programador p1 = new Programador("rafa", 100);
        p1.linguagemDeProgramacao = "Java";
        System.out.println(p1.getNome());
        System.out.println(p1.getSalario());
        System.out.println(p1.getBonificacao());
        System.out.println(p1.linguagemDeProgramacao);

        System.out.println("");

        System.out.println("Terceiro");
        Funcionario p2 = new Programador("matheus", 200);
        Programador polimorfismo1 = (Programador) p2; // Polimorfismo
        System.out.println(polimorfismo1.getNome());
        System.out.println(polimorfismo1.getSalario());
        System.out.println(polimorfismo1.getBonificacao());
        System.out.println(polimorfismo1.linguagemDeProgramacao);

        System.out.println("");

        System.out.println("Quarto");
        Funcionario f1 = p1; // Polimorfismo
        System.out.println(f1.getNome());
        System.out.println(f1.getSalario());
        System.out.println(f1.getBonificacao());
        // Não dá para chamar a linguagem de programação

        System.out.println("");

        System.out.println("Quinto");
        Secretario secretario = new Secretario("rafa", 100);
        Programador programador = new Programador("guizao", 200);
        Presidente presidente = new Presidente("guizao", 1000, "12345");
        System.out.println("Bonificacao: " + secretario.getBonificacao());
        System.out.println("Bonificacao: " + programador.getBonificacao());
        System.out.println("Bonificacao: " + presidente.getBonificacao());
        TotalizadorDeBonificacoes totalizadorDeBonificacoes = new TotalizadorDeBonificacoes();
        totalizadorDeBonificacoes.calcularBonificacao(secretario);
        totalizadorDeBonificacoes.calcularBonificacao(programador);
        totalizadorDeBonificacoes.calcularBonificacao(presidente);
        System.out.println("Total:" + totalizadorDeBonificacoes.getTotalGastoEmBonificacoes());

        System.out.println("");

        System.out.println("Sexto");
        Diretor bruna = new Diretor("bruna", 50, "1234");
        Funcionario func1 = bruna;
        Autenticavel a1 = bruna; // Interface
        FuncionarioAutenticavel fa1 = bruna;
        Presidente gui = new Presidente("gui", 1000, "12345");
        Funcionario func2 = gui;
        Autenticavel a2 = gui; // Interface
        FuncionarioAutenticavel fa2 = gui;
        Cliente cliente = new Cliente("rafa", "123");
        Autenticavel a3 = cliente; // Interface

        Seguranca seguranca = new Seguranca();
        seguranca.autenticarFuncionarios(gui);
        seguranca.autenticarFuncionarios(bruna);
        seguranca.autenticarFuncionarios(cliente);

        // Todos que possuem a interface Autenticável tem o método OlaMundo (menos a classe Funcionario)
        System.out.println(bruna.OlaMundo());
        System.out.println(a1.OlaMundo());
        System.out.println(fa1.OlaMundo());
        System.out.println(gui.OlaMundo());
        System.out.println(a2.OlaMundo());
        System.out.println(fa2.OlaMundo());
        System.out.println(a3.OlaMundo());

        System.out.println("");

        System.out.println("Sétimo");
        new A(){}.m1();
        new B(){}.m1();
        new C(){}.m1();
        new D(){}.m1();
    }
}
