package main.java.main;

/**
 * Interface é um contrato que obriga a quem implementa seguir as implementações
 * **/
public interface Autenticavel {
    public String getSenhaDeAutenticacao();

    // Com o 'default' não é obrigado seguir a implementação
    public default String OlaMundo() {
        return "Ola mundo";
    }
}
