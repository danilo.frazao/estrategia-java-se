package main.java.main;

public class IncrementoDeslocamento {
    public static void main (String args []){
        int x = 5;
        int z = x << 2;
        System.out.println(x++);
        System.out.println(++x);
        System.out.println(z);
        z >>= 2;
        System.out.println(z);
        System.out.println("");

        x = 5;
        int y = x++;
        x = 5;
        int w = ++x;
        System.out.println(y);
        System.out.println(w);
        System.out.println("");

        int dif = 10;
        dif += 1;
        System.out.println(dif);
        dif =- 1;
        System.out.println(dif);
    }
}
